import json
from flask import request
from flask import Flask, render_template
import urllib
from flask import Flask, session

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.route("/", methods = ['GET'])
def index():
    return render_template("index.html")

@app.route("/static/search_library", methods = ['POST'])
def search_library():
    district = request.form['district']
    computers = request.form['computers']
    wifi = request.form['wifi']
    if wifi.lower() == "yes":
        wifi = "Yes"
    elif wifi.lower() == "no":
        wifi = "No"
    request_url = "https://data.austintexas.gov/resource/tc36-hn4j.json"
    r = urllib.request.urlopen(request_url)
    data = json.loads(r.read())
    require = {}
    if wifi != "":
        require["wifi"] = wifi
    if computers != "":
        require["computers"] = computers
    if district != "":
        require["district"] = district

    if len(require) == 0:
        output = []
        for each in data:
            if "computers" in each:
                output.append(
                    {"name": each["name"], "phone": each["phone"], "human_address": each["address"]["human_address"],
                     "district": each["district"], "wifi": each["wifi"], "computers": each["computers"]})
            else:
                output.append(
                    {"name": each["name"], "phone": each["phone"], "human_address": each["address"]["human_address"],
                     "district": each["district"], "wifi": each["wifi"]})
        session['result'] = output
        return render_template('lib.html')
    output = []
    for each in data:
        c = 1
        for k, v in require.items():
            if k in each:
                if each[k] != v:
                    c = 0
            else:
                c = 0
        if (c == 1):
            if "computers" in each:
                output.append({"name": each["name"], "phone": each["phone"], "human_address": each["address"]["human_address"],
                     "district": each["district"], "wifi": each["wifi"], "computers": each["computers"]})
            else:
                output.append({"name": each["name"], "phone": each["phone"], "human_address": each["address"]["human_address"],
                           "district": each["district"], "wifi": each["wifi"]})
    session['result'] = output
    return render_template('lib.html')


@app.route("/library", methods=['GET'])
def library():
    output = session.pop('result', None)
    return json.dumps(output)




